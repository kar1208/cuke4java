# Cucumber For Java Sample Project
[![Build Status](http://hq-rhp-6007.labs.addev.ssa.gov:8080/buildStatus/icon?job=cuke4java/MasterBranchBuild)](http://hq-rhp-6007.labs.addev.ssa.gov:8080/job/cuke4java/job/MasterBranchBuild/)

Adapted from "The Cucumber for Java Book"

Example program uses Cucumber scenarios, and embedded Selenium testing.