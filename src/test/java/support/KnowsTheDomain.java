package support;

import java.net.MalformedURLException;

import nicebank.Account;
import nicebank.CashSlot;
import nicebank.Teller;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class KnowsTheDomain {
	private Account myAccount;
	private CashSlot cashSlot;
	private Teller teller;
	private EventFiringWebDriver webDriver;
	private WebDriver remoteWebDriver;
	
	public Account getMyAccount() {
		if (myAccount == null) {
			myAccount = new Account();
		}
		
		return myAccount;
	}
	
	public Teller getTeller() throws MalformedURLException {
		if (teller == null) {
			teller = new AtmUserInterface();
		}
		
		return teller;
	}
	
	public CashSlot getCashSlot() {
		if (cashSlot == null) {
			cashSlot = new CashSlot();
		}
		
		return cashSlot;
	}
	
}
