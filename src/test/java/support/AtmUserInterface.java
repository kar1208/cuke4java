package support;

import java.net.MalformedURLException;
import java.net.URL;

import hooks.ServerHooks;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.WebDriverException;

import nicebank.Account;
import nicebank.Teller;

 class AtmUserInterface implements Teller {
	 
	 public EventFiringWebDriver webDriver;
	 
	 public AtmUserInterface() throws MalformedURLException{

		 //TODO Support Parallel Execution with Multiple Browsers
		 String testType = System.getProperty("seleniumTestType","local");
		 if (testType.equals("local")) {
			 this.webDriver = new EventFiringWebDriver(new FirefoxDriver());
		 } else if (testType.equals("headless")){
			 this.webDriver = new EventFiringWebDriver(new HtmlUnitDriver());
		 
		 } else if (testType.equals("hub")) {
			 DesiredCapabilities dc = DesiredCapabilities.firefox();
			 dc.setBrowserName("firefox");
			 String hubURL = System.getProperty("hubURL");
			 RemoteWebDriver rwd = new RemoteWebDriver(new URL(hubURL), dc);
			 EventFiringWebDriver efwd = new EventFiringWebDriver(rwd);		
		     this.webDriver = efwd;
		 }

	 }

	 public void withdrawFrom(Account account, int dollars) {
		 try {
			 webDriver.get("http://" + ServerHooks.hostname + ":" + ServerHooks.PORT);
			 webDriver.findElement(By.id("amount"))
			 		  .sendKeys(String.valueOf(dollars));
			 webDriver.findElement(By.id("withdraw")).click();
		 }
		 finally {
			 webDriver.quit();
		 }
	 }
}
