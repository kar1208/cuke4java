package hooks;

import java.net.InetAddress;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.Scenario;
import nicebank.AtmServer;
import support.KnowsTheDomain;

public class ServerHooks {
	public static final int PORT = 8887;
	
	public static String hostname = getCanonicalHostname();
	
	private AtmServer server;
	private KnowsTheDomain helper;
	
	public static String getCanonicalHostname() {
		try {
			return InetAddress.getLocalHost().getCanonicalHostName();
		} catch (Exception e) {
			return "localhost";
		}
	}
	
    public ServerHooks(KnowsTheDomain helper) {
        this.helper = helper;
    }
	
	@Before
	public void startServer() throws Exception {
		server = new AtmServer(PORT, helper.getCashSlot(), helper.getMyAccount());
		server.start();
	}
	
	@After
	public void stopServer() throws Exception {
		server.stop();
	}
}
